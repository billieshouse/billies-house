Billies House is a boutique flower, gifts, plants, decor shop located in downtown Squamish, BC. Billies offers same day delivery to Squamish, Furry Creek and Britannia Beach and 24hr notice for deliveries to Whistler and Pemberton. Our house is your house.

Address: 38082 Cleveland Ave, Squamish, BC V8B 0B2, Canada

Phone: 604-892-9232

Website: [https://billieshouse.com](https://billieshouse.com)
